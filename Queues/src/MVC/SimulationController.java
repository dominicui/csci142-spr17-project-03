package MVC;
/**
 * UI controller
 */
import Model.Cashier;
import Model.CustomerGenerator;
import Model.ServiceQueueManager;

public class SimulationController implements Runnable
{
    private SimulationView myView;
    private ServiceQueueManager myModel;
    private Cashier[] myCashiers;
    private CustomerGenerator myGenerator;
    private Thread myThread;
    private int myCashierCount, myMaxServiceTime, myMaxGenerateTime, myNumberOfCustomers;
    private boolean mySuspend, myKillSignal, myHasRun, mySuspendSignal;

    public static void main(String[] args)
    {
        new SimulationController();
    }

    public SimulationController()
    {
        myView = new SimulationView(this);
        myModel = new ServiceQueueManager();
        myCashierCount = -1;
        myMaxServiceTime = -1;
        myMaxGenerateTime = -1;
        myNumberOfCustomers = -1;
        myThread = new Thread(this);
        mySuspend = false;
        myKillSignal = false;
        myHasRun = false;
    }

    /**
     * start running
     */
    public synchronized void startSimulation()
    {   
    	//start new statistic
        if (myHasRun)
        {
            for (int i = 0; i < myCashiers.length; i++)
            {
                myCashiers[i].kill();
            }
            myGenerator.kill();
        }

        //get data
        myView.switchGoPauseText();
        myCashierCount = myView.getCashierCount();
        myMaxServiceTime = myView.getMaxServiceTime();
        myMaxGenerateTime = myView.getMaxGenerateTime(); 
        myNumberOfCustomers = myView.getCustomerCount();
        
        myCashiers = new Cashier[myCashierCount];
        
        myModel.cleanQueues();
        myModel.generateQueues(myCashierCount);
        
        for (int i = 0; i < myCashierCount; i++)
        {
            myCashiers[i] = new Cashier(myMaxServiceTime, myModel.getQueue(i));
            myCashiers[i].start();
        }
        myView.updateView(true);
        myModel.setCashiers(myCashiers);
        System.gc();
                
        myGenerator = new CustomerGenerator(myMaxGenerateTime, myNumberOfCustomers, myModel);
        myGenerator.start();
        
        myModel.resetStatistics();
        if (!myHasRun) 
        {
            this.start();
            myHasRun = true;
        }
        else
        {
            this.resume();
        }
    }
	/**
	 * change button text
	 */
    public void goAndPause()
    {
        if (myView.getGoPauseText() == SimulationView.GO_TEXT)
        {
            if (myCashierCount == myView.getCashierCount() && myMaxServiceTime == myView.getMaxServiceTime()
                    && myMaxGenerateTime == myView.getMaxGenerateTime() && myNumberOfCustomers == myView.getCustomerCount())
            {
                this.resumeSimulation();
                myView.switchGoPauseText();
            }
            else
            {
                this.startSimulation();
            }
        }
        else
        {
            mySuspendSignal = true;
            myView.switchGoPauseText();
        }
    }

    @Override
    public void run()
    {
        while (true)
        {
            try
            {
                this.waitWhileSuspended();
            } 
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            if (myKillSignal) 
            {
                myHasRun = false;
                myKillSignal = false;
                break;
            }
            if (mySuspendSignal) this.suspendSimulation();
            if (!mySuspendSignal) this.updateView();
        }
    }

    /**
     * update statistic
     */
    public void updateView()
    {
        synchronized (myGenerator)
        {
            myView.updateView(false);
        }
        try
        {
            Thread.sleep(5);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        if (myGenerator.getFinished() && myCashierCount != -1)
        {
            myView.updateView(false);
            this.suspendSimulation();
            myView.switchGoPauseText();
            myCashierCount = -1;
        }

        this.getModel().computeStatistics();
        myView.myTotalStats.setText("Customers Served:\n" + this.getModel().getCustomersServed() + "\n"
                + "Total Wait Time:\n" + this.getModel().getTotalWaitTime() + "ms\n"
                + "Total Service Time:\n" + this.getModel().getTotalServiceTime() + "ms\n"
                + "Total Idle Time:\n" + this.getModel().getTotalIdleTime() + "ms\n"
                + "Average Wait Time:\n" + this.getModel().getAverageWaitTime() + "ms\n"
                + "Average Service Time:\n" + this.getModel().getAverageServiceTime() + "ms\n"
                + "Average Idle Time:\n" + this.getModel().getAverageIdleTime() + "ms");
        if (myView.getMyCurrentCashier() != -1)
        {
            int fixedCashier = myView.getMyCurrentCashier() + 1;
            myView.myCashierStats.setText("Cashier #" + fixedCashier + "\n" + 
                    "Customers Served:\n" + this.getModel().getQueue(myView.getMyCurrentCashier()).getCustomersServed() + "\n" + 
                    "Total Wait Time:\n" + this.getModel().getQueue(myView.getMyCurrentCashier()).getWaitTime() + "ms\n"
                    + "Total Service Time:\n" + this.getModel().getQueue(myView.getMyCurrentCashier()).getServiceTime() + "ms\n"
                    + "Total Idle Time:\n" + this.getModel().getQueue(myView.getMyCurrentCashier()).getIdleTime() + "ms\n"
                    + "Average Wait Time:\n" + this.getModel().getQueue(myView.getMyCurrentCashier()).getWaitTime()/this.getModel().getQueue(myView.getMyCurrentCashier()).getCustomersServed() + "ms\n"
                    + "Average Service Time:\n" + this.getModel().getQueue(myView.getMyCurrentCashier()).getServiceTime()/this.getModel().getQueue(myView.getMyCurrentCashier()).getCustomersServed() + "ms\n"
                    + "Average Idle Time:\n" + this.getModel().getQueue(myView.getMyCurrentCashier()).getIdleTime()/this.getModel().getQueue(myView.getMyCurrentCashier()).getCustomersServed() + "ms");
        }
    }
    /**
     * getters, setters and other helpful method 
     */
    public synchronized void resume()
    {
        mySuspend = false;
        mySuspendSignal = false;
        this.notify();
    }

    public void suspend()
    {
        mySuspend = true;
    }
      
    public void kill()
    {
        myKillSignal = true;
        if (mySuspend == true) this.resume();
    }

    public void start()
    {
        try
        {
            myThread.start();
        }
        catch(IllegalThreadStateException e)
        {
            System.out.println("Thread already started");
        }
    }

    public synchronized void waitWhileSuspended() throws InterruptedException
    {
        while (mySuspend)
        {
            this.wait();
        }
    }

    public int getCashierCount()
    {
        return myCashierCount;
    }

    public int getMaxGenerateTime()
    {
        return myMaxGenerateTime;
    }

    public int getCustomerCount()
    {
        return myNumberOfCustomers;
    }

    public int getMaxServiceTime()
    {
        return myMaxServiceTime;
    }

    public int getQueues()
    {
        if (myModel == null) return 0;
        return myModel.getQueueCount();
    }
  
    public int getQueueSize(int i)
    {
        return myModel.getQueue(i).size();
    }

    public ServiceQueueManager getModel()
    {
        return myModel;
    }
 
    public void suspendSimulation()
    {
        this.suspend();
        myGenerator.suspend();
        for (int i = 0; i < myCashiers.length; i++)
        {
            myCashiers[i].suspend();
        }
    }
   
    public void resumeSimulation()
    {
        mySuspendSignal = false;
        this.resume();
        for (int i = 0; i < myCashiers.length; i++)
        {
            myCashiers[i].resume();
        }
        myGenerator.resume();
    }

    public void setCashierView(Integer i)
    {
        myView.setCurrentCashier(i);
        this.updateView();
    }
}