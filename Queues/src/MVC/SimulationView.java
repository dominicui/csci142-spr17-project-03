package MVC;

/**
 * user interface
 * 
 * Dominic
 */
import java.awt.*;
import java.lang.reflect.Method;

import javax.swing.*;

import Model.ServiceQueueManager;

public class SimulationView
{
    public static final String GO_TEXT = "Go!";
    public static final String PAUSE_TEXT = "Pause";
    
    private int myCurrentCashier;
    private JFrame myFrame;
    private SimulationController myController;
    private JTextField[] myOverflows;
    private JTextField myGenerateEntry, myNumEntry, myServiceEntry, myCashierNum;
    JTextArea myTotalStats;
	JTextArea myCashierStats;
    private JLabel myGenerateLabel, myNumLabel, myServiceLabel, myCashierLabel;
    private JButton myGoButton;
    private JLabel[] myCashiers;
    private JLabel[][] myCustomers;
    private ButtonListener[] myCashierListener;    
    private ButtonListener myGoPauseListener;

    /**
     * set up view
     */
    
    public SimulationView(SimulationController controller)
    {
        myFrame = new JFrame();
        myFrame.setSize(1000,800);
        myFrame.setLayout(null);
              
        myController = controller;
        /**
         * input
         */
        myNumLabel = new JLabel("Number of Customers: ");
        myNumLabel.setLocation(770, 550);
        myNumLabel.setSize(200, 20);
        myFrame.add(myNumLabel);

        myNumEntry = new JTextField();
        myNumEntry.setLocation(770, 570);
        myNumEntry.setSize(200, 20);
        myFrame.add(myNumEntry);
        
        myGenerateLabel = new JLabel("Generation Time: ");
        myGenerateLabel.setLocation(770, 590);
        myGenerateLabel.setSize(200, 20);
        myFrame.add(myGenerateLabel);
       
        myGenerateEntry = new JTextField();
        myGenerateEntry.setLocation(770, 610);
        myGenerateEntry.setSize(200, 20);
        myFrame.add(myGenerateEntry);

        myServiceLabel = new JLabel("Max Service Time: ");
        myServiceLabel.setLocation(770, 630);
        myServiceLabel.setSize(200, 20);
        myFrame.add(myServiceLabel);

        myServiceEntry = new JTextField();
        myServiceEntry.setLocation(770, 650);
        myServiceEntry.setSize(200, 20);
        myFrame.add(myServiceEntry);
        
        myCashierLabel = new JLabel("Number of Cashiers 1-5: ");
        myCashierLabel.setLocation(770, 670);
        myCashierLabel.setSize(200, 20);
        myFrame.add(myCashierLabel);
        
        myCashierNum = new JTextField();
        myCashierNum.setLocation(770, 690);
        myCashierNum.setSize(200, 20);
        myFrame.add(myCashierNum);
        
        myGoButton = new JButton("Go!");
        myGoButton.setLocation(830, 720);
        myGoButton.setSize(80, 30);     
        myFrame.add(myGoButton);
        /**
         * Statistics
         */
        myTotalStats = new JTextArea("Start to see complete statistics...");
        myTotalStats.setLocation(750, 0);
        myTotalStats.setSize(250, 250);
        myTotalStats.setDisabledTextColor(Color.BLACK);
        myFrame.add(myTotalStats);

        myCashierStats = new JTextArea("Click on a cashier to see their individual statistics...(what ever it is running or completed)");
        myCashierStats.setLocation(750, 270);
        myCashierStats.setSize(250, 280);
        myCashierStats.setWrapStyleWord(true);
        myCashierStats.setEnabled(false);
        myCashierStats.setDisabledTextColor(Color.BLACK);
        myFrame.add(myCashierStats);
        myCashierStats.setLineWrap(true);
        /**
         * images' location
         */
        myCashierListener = new ButtonListener[5];

        myOverflows = new JTextField[ServiceQueueManager.MAX_NUMBER_OF_QUEUES];
        myCashiers = new JLabel[ServiceQueueManager.MAX_NUMBER_OF_QUEUES];
        for (int i = 0; i < myCashiers.length; i++)
        {
            myOverflows[i] = new JTextField("+0");
            myOverflows[i].setSize(70, 24);
            myOverflows[i].setLocation(150 * i, 0);

            myOverflows[i].setDisabledTextColor(Color.BLACK);
            myOverflows[i].setHorizontalAlignment(JTextField.CENTER);
            myFrame.add(myOverflows[i]);

            
            myCashiers[i] = new JLabel(new ImageIcon("images/cashier.jpg"));
            myCashiers[i].setSize(100, 200);
            myCashiers[i].setLocation(150 * i, 600);
            myFrame.add(myCashiers[i]);

        }

        myCustomers = new JLabel[5][7];
        for (int j = 0; j < myCustomers[0].length; j++)
        {
            for (int i = 0; i < myCustomers.length; i++)
            {
                myCustomers[i][j] = new JLabel(new ImageIcon("images/face.jpg"));
                myCustomers[i][j].setSize(100, 70);
                myCustomers[i][j].setLocation(150 * i, (70 * j) +30);
                myFrame.add(myCustomers[i][j], 10);
                myCustomers[i][j].setVisible(false);
            }
        }

        setMyCurrentCashier(-1);

        this.associateListeners();
        myFrame.setVisible(true);
        myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
    /**
     * set visibility of customers images
     * @param reconstruct
     */
    public synchronized void updateView(boolean reconstruct)
    {
        for (int i = 0; i < myController.getCashierCount(); i++)
        {
            if (myController.getQueueSize(i) < 7)
            {
                myOverflows[i].setText("+0");
                for (int j = 0; j < myController.getQueueSize(i); j++)
                {
                    myCustomers[i][6 - j].setVisible(true);
                }
                for (int j = myController.getQueueSize(i); j < 7; j++)
                {
                    myCustomers[i][6 - j].setVisible(false);
                }
            }
            else
            {
                for (int j = 0; j < 7; j++)
                {
                    myCustomers[i][j].setVisible(true);
                }
                int myOverflowCount = myController.getQueueSize(i) - 7;
                myOverflows[i].setText("+" + myOverflowCount);
            }
        }
       
        
    }

    /**
     * associateListeners
     */
    private void associateListeners()
    {
        Class<? extends SimulationController> controlClass;
        Method goAndPauseMethod;
        Method cashierMethod;
        Class<?>[] classArgs;
        
        controlClass = myController.getClass();
        goAndPauseMethod = null;
        cashierMethod = null;
        classArgs = new Class[1];
        
        try
        {
           classArgs[0] = Class.forName("java.lang.Integer");
        }
        catch(ClassNotFoundException e)
        {
           String error;
           error = e.toString();
           System.out.println(error);
        }
        
        try
        {
            goAndPauseMethod = controlClass.getMethod("goAndPause", (Class<?>[])null);
            cashierMethod = controlClass.getMethod("setCashierView", classArgs);
        } 
        catch (NoSuchMethodException e)
        {
            e.printStackTrace();
        } 
        catch (SecurityException e)
        {
            e.printStackTrace();
        }
        
        myGoPauseListener = new ButtonListener(myController, goAndPauseMethod, null);
        myGoButton.addMouseListener(myGoPauseListener);
        Integer[] args;
        for (int i = 0; i < 5; i++)
        {
            args = new Integer[1];
            args[0] = new Integer(i);
            myCashierListener[i] = new ButtonListener(myController, cashierMethod, args);
            myCashiers[i].addMouseListener(myCashierListener[i]);
        }
    }

    /**
     * getters help getting data
     * @return
     */
    public int getCashierCount()
    {
        return Integer.parseInt(myCashierNum.getText());
    }

    public int getMaxGenerateTime()
    {
        if (!myGenerateEntry.getText().matches("[0-9]++")) return -1;
        int generateTime = Integer.parseInt(myGenerateEntry.getText());
        if (generateTime <= 0) return -1;
        return generateTime;
    }

    public int getCustomerCount()
    {
        if (!myNumEntry.getText().matches("[0-9]++")) return -1;
        int customers = Integer.parseInt(myNumEntry.getText());
        if (customers <= 0) return -1;
        return customers;
    }
        
    public int getMaxServiceTime()
    {
        if (!myServiceEntry.getText().matches("[0-9]++")) return -1;
        int service = Integer.parseInt(myServiceEntry.getText());
        if (service <= 0) return -1;
        return service;
    }
  
    public String getGoPauseText()
    {
        return myGoButton.getText();
    }

    public void switchGoPauseText()
    {
        if (this.getGoPauseText().equals(SimulationView.GO_TEXT))
        {
            myGoButton.setText(SimulationView.PAUSE_TEXT);
        }
        else
        {
            myGoButton.setText(SimulationView.GO_TEXT);
        }
    }

    public void setCurrentCashier(int currentCashier)
    {
        setMyCurrentCashier(currentCashier);
    }

	public int getMyCurrentCashier() {
		return myCurrentCashier;
	}

	public void setMyCurrentCashier(int myCurrentCashier) {
		this.myCurrentCashier = myCurrentCashier;
	}
}