package Model;
/**
 * customer's parent class
 */
import java.util.Random;

public class UniformCustomerGenerator
{
    private int myRandom;
    private int myMaxGenerationTime;

    public UniformCustomerGenerator(int maxGenerationTime, ServiceQueueManager serviceQueueManager)
    {
        myMaxGenerationTime = maxGenerationTime;
        myRandom = 0;
    }

    public int generateTime()
    {
        myRandom = new Random().nextInt(myMaxGenerationTime);
        return myRandom;
    }
}
