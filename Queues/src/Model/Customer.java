package Model;
/**
 * set conditions of customer
 * serve time, enter time and wait time
 * @author dominic
 *
 */
public class Customer
{
    private long myServiceTime, myEntryTime, myWaitTime;

    public Customer()
    {
        myServiceTime = -1;
        myWaitTime = -1;
        myEntryTime = System.nanoTime();
    }

    /**
     * setters and getters
     * @param serviceTime
     */
    public void setServiceTime(int serviceTime)
    {
        myServiceTime = serviceTime;
    }
 
    public void setEntryTime(int entryTime)
    {
        myEntryTime = entryTime;
    }
 
    public void setWaitTime(int waitTime)
    {
        myWaitTime = waitTime;
    }
 
    public long getServiceTime()
    {
        return myServiceTime;
    }
 
    public long getEntryTime()
    {
        return myEntryTime;
    }

    public long getWaitTime()
    {
        myWaitTime = (System.nanoTime() - myEntryTime)/1000000;
        return myWaitTime;
    }
}
