package Model;
/**
 * Cashier's parent class
 */
import java.util.Random;

public class UniformCashier
{
    private int myMaxTimeService;
    private int myRandom;
    
    public UniformCashier(int maxServiceTime, ServiceQueue serviceQueue)
    {
        myMaxTimeService = maxServiceTime;
        myRandom = 0;
    }

    public int generateTime()
    {
        myRandom = new Random().nextInt(myMaxTimeService);
        return myRandom;
    }
}
