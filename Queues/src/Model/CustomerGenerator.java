package Model;
/**
 * customer line as queue, run as threads
 * @author dominic
 *
 */
public class CustomerGenerator extends UniformCustomerGenerator implements Runnable
{
    private ServiceQueueManager myManager;
    private Thread myThread;
    private int myCustomerCount, myMaxCustomers;
    private boolean mySuspend, myKillSignal, myFinished;

    public CustomerGenerator(int maxGenerationTime, int maxCustomers, ServiceQueueManager serviceQueueManager)
    {
        super(maxGenerationTime, serviceQueueManager);
        myManager = serviceQueueManager;
        myThread = new Thread(this);
        mySuspend = false;
        myKillSignal = false;
        myMaxCustomers = maxCustomers;
        myCustomerCount = 0;
        myFinished = false;
    }

    /**
     * thread
     */
    @Override
    public void run()
    {
        while (myCustomerCount < myMaxCustomers)
        {
            try
            {
                this.waitWhileSuspended();
            } 
            catch (InterruptedException e1)
            {
                e1.printStackTrace();
            }
            if (myKillSignal) break;
            int generateTime = this.generateTime();
            try
            {
                
                Thread.sleep(generateTime);
            } 
            catch (InterruptedException e)
            {
                System.out.println("Thread interrupted");
            }
            this.insertCustomer();
            myCustomerCount++;
        }
        if (myCustomerCount == myMaxCustomers)
        {
            int numOfQueues = myManager.getQueueCount();
            for (int i = 0; i < numOfQueues; i++) 
            {
                while (myManager.getQueue(i).size() != 0)
                {
                    try
                    {
                        Thread.sleep(0);
                    } catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
            myManager.sleepAllCashiers(); 
            myFinished = true;
        }
    }

    public synchronized void insertCustomer()
    {
        Customer newCustomer = new Customer();
        myManager.determineShortestQueue().insertCustomer(newCustomer);
    }

    public void start()
    {
        try
        {
            myThread.start();
        }
        catch(IllegalThreadStateException e)
        {
            System.out.println("Thread already started");
        }
    }

    private synchronized void waitWhileSuspended() throws InterruptedException
    {
        while (mySuspend)
        {
            this.wait();
        }
    }

    public void kill()
    {
        myKillSignal = true;
        if (mySuspend == true) this.resume();
    }

    public void suspend()
    {
        mySuspend = true;
    }
    
    public synchronized void resume()
    {
        mySuspend = false;
        this.notify();
    }

    public boolean getFinished()
    {
        return myFinished;
    }
}
