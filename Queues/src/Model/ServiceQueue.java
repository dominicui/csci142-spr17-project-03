package Model;
/**
 * queue based on Linkedlist 
 */
import java.util.LinkedList;

@SuppressWarnings("serial")
public class ServiceQueue extends LinkedList<Customer>
{
    private int myCustomersServed;
    private boolean myStatus;
    private float myIdleTime, myServiceTime, myWaitTime;

    public ServiceQueue()
    {
        myCustomersServed = 0;
        myStatus = true;
        myIdleTime = 0;
        myServiceTime = 0;
        myWaitTime = 0;
    }

    public synchronized void insertCustomer(Customer customer)
    {
        this.addFirst(customer);
    }

    public synchronized Customer serveCustomer()
    {
        myCustomersServed++;
        return this.removeLast();
    }
    /**
     * collect data
     * @param newStatus
     */
    public void setStatus(boolean newStatus)
    {
        myStatus = newStatus;
    }

    public boolean getStatus()
    {
        return myStatus;
    }

    public void addIdleTime(float addition)
    {
        myIdleTime += addition;
    }

    public void addServiceTime(float addition)
    {
        myServiceTime += addition;
    }

    public void addWaitTime(float addition)
    {
        myWaitTime += addition;
    }
 
    public int getCustomersServed()
    {
        return myCustomersServed;
    }

    public synchronized void resetStatistics()
    {
        myIdleTime = 0;
        myWaitTime = 0;
        myServiceTime = 0;
        myCustomersServed = 0;
    }

    public float getIdleTime()
    {
        return myIdleTime;
    }

    public float getServiceTime()
    {
        return myServiceTime;
    }

    public float getWaitTime()
    {
        return myWaitTime;
    }
}
