package Model;
/**
 * Cashier class serve customer from queue
 * run as threads
 * @author dominic
 *
 */
public class Cashier extends UniformCashier implements Runnable 
{
    private ServiceQueue myServiceQueue;
    private Thread myThread;
    private boolean mySuspend, myKillSignal;
    private long mySuspendTime, myIdleTime;

    public Cashier(int maxServiceTime, ServiceQueue serviceQueue)
    {
        super(maxServiceTime, serviceQueue);
        myServiceQueue = serviceQueue;
        myThread = new Thread(this);
        myKillSignal = false;
        myIdleTime = 0;
        mySuspendTime = -1;
        mySuspend = false;
    }
	/**
     * random serves time
     */
    public void serveCustomer()
    {
        int myTime = this.generateTime();
        try
        {
            Thread.sleep(myTime);
        } 
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        Customer customerOff = myServiceQueue.serveCustomer();
        myServiceQueue.addWaitTime(customerOff.getWaitTime() - myTime);
        customerOff.setServiceTime(myTime);
        myServiceQueue.addServiceTime(customerOff.getServiceTime());
        System.gc();
    }

    public void start()
    {
        try
        {
            myThread.start();
        }
        catch(IllegalThreadStateException e)
        {
            System.out.println("Thread already started");
        }
    }

    @Override
    public void run()
    {
        while (true)
        {
            try
            {
                this.waitWhileSuspended();
            } 
            catch (InterruptedException e)
            {
                System.out.println("Thread suspended.");
            }
            
            if (myServiceQueue.size() == 0 && mySuspendTime == -1)
            {
                mySuspendTime = System.nanoTime();
            }
            
            if (myKillSignal) break;
            
            if (myServiceQueue.size() != 0)
            {
                this.serveCustomer();
                if (mySuspendTime > 0)
                {
                    long addToIdle = System.nanoTime() - mySuspendTime;
                    myServiceQueue.addIdleTime(addToIdle/1000000);
                    myIdleTime = 0;
                    mySuspendTime = -1;
                }
            }
        }
    }
 
    private synchronized void waitWhileSuspended() throws InterruptedException
    {
        while (mySuspend)
        {
            this.wait();
        }
    }

    public void suspend()
    {
        mySuspend = true;
    }
   
    public boolean getStatus()
    {
        return mySuspend;
    }
   
    public void kill()
    {
        myKillSignal = true;
        if (mySuspend == true) this.resume();
    }
    
    public long getIdleTime()
    {
        return myIdleTime;
    }
    
    public synchronized void resume()
    {
        mySuspend = false;
        this.notify();
    }
}
